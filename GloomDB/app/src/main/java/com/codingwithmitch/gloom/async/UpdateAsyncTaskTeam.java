package com.codingwithmitch.gloom.async;

import android.os.AsyncTask;

import com.codingwithmitch.gloom.models.Team;
import com.codingwithmitch.gloom.persistence.TeamsDao;

public class UpdateAsyncTaskTeam extends AsyncTask<Team, Void, Void> {

    private TeamsDao mTeamsDao;

    public UpdateAsyncTaskTeam(TeamsDao dao) {
        mTeamsDao = dao;
    }

    @Override
    protected Void doInBackground(Team... teams) {
        mTeamsDao.updateNotes(teams);
        return null;
    }

}