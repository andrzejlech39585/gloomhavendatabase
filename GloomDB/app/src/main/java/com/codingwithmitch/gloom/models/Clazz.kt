package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "clazzes")
class Clazz : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "ClassName")
    var clazzName: String? = null


    constructor(clazzName: String?) {
        this.clazzName = clazzName
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        clazzName = `in`.readString()
    }

    override fun toString(): String {
        return "Note{" +
                "ClassName='" + clazzName + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(clazzName)
    }

    companion object CREATOR : Parcelable.Creator<Clazz> {
        override fun createFromParcel(parcel: Parcel): Clazz {
            return Clazz(parcel)
        }

        override fun newArray(size: Int): Array<Clazz?> {
            return arrayOfNulls(size)
        }
    }
}
