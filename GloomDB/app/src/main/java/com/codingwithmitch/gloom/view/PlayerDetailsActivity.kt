package com.codingwithmitch.gloom.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.OnDoubleTapListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.EditText
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.boss.gloom.R
import com.codingwithmitch.gloom.models.Player
import com.codingwithmitch.gloom.persistence.PlayerNoteRepository
import com.codingwithmitch.gloom.util.LinedEditText
import com.codingwithmitch.gloom.util.Utility


class PlayerDetailsActivity : AppCompatActivity(), OnTouchListener, GestureDetector.OnGestureListener, OnDoubleTapListener, View.OnClickListener, TextWatcher {
    // UI components
    private var mLinedEditText: LinedEditText? = null
    private var mEditTitle: EditText? = null
    private var mViewTitle: TextView? = null
    private var mCheckContainer: RelativeLayout? = null
    private var mBackArrowContainer: RelativeLayout? = null
    private var mCheck: ImageButton? = null
    private var mBackArrow: ImageButton? = null
    // vars
    private var mIsNewNote = false
    lateinit var mNoteInitial: Player
    private var mGestureDetector: GestureDetector? = null
    private var mMode = 0
    private var mNoteRepository: PlayerNoteRepository? = null
    private var mNoteFinal: Player? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.team_details)

        mEditTitle = findViewById(R.id.note_edit_title)
        mViewTitle = findViewById(R.id.note_text_title)
        mCheck = findViewById(R.id.toolbar_check)
        mBackArrow = findViewById(R.id.toolbar_back_arrow)
        mCheckContainer = findViewById(R.id.check_container)
        mBackArrowContainer = findViewById(R.id.back_arrow_container)
        mNoteRepository = PlayerNoteRepository(this)
        setListeners()
        if (incomingIntent) {
            setNewNoteProperties()
            enableEditMode()
        } else {
            setNoteProperties()
            disableContentInteraction()
        }
    }

    private fun saveChanges() {
        if (mIsNewNote) {
            saveNewPlayer()
        } else {
            updatePlayer()
        }
    }

    fun updatePlayer() {
        mNoteRepository!!.updatePlayerTask(mNoteFinal)
    }

    fun saveNewPlayer() {
        mNoteRepository!!.insertPlayerTask(mNoteFinal)
    }

    private fun setListeners() {
        mGestureDetector = GestureDetector(this, this)
        mLinedEditText!!.setOnTouchListener(this)
        mCheck!!.setOnClickListener(this)
        mViewTitle!!.setOnClickListener(this)
        mBackArrow!!.setOnClickListener(this)
        mEditTitle!!.addTextChangedListener(this)
    }

    private val incomingIntent: Boolean
        private get() {
            if (intent.hasExtra("selected_note")) {
                mNoteInitial = intent.getParcelableExtra("selected_note")
                mNoteFinal = Player()
                mNoteFinal!!.playerName = mNoteInitial.playerName
                mNoteFinal!!.id = mNoteInitial.id
                mMode = EDIT_MODE_ENABLED
                mIsNewNote = false
                return false
            }
            mMode = EDIT_MODE_ENABLED
            mIsNewNote = true
            return true
        }

    private fun disableContentInteraction() {
        mLinedEditText!!.keyListener = null
        mLinedEditText!!.isFocusable = false
        mLinedEditText!!.isFocusableInTouchMode = false
        mLinedEditText!!.isCursorVisible = false
        mLinedEditText!!.clearFocus()
    }

    private fun enableContentInteraction() {
        mLinedEditText!!.keyListener = EditText(this).keyListener
        mLinedEditText!!.isFocusable = true
        mLinedEditText!!.isFocusableInTouchMode = true
        mLinedEditText!!.isCursorVisible = true
        mLinedEditText!!.requestFocus()
    }

    private fun enableEditMode() {
        mBackArrowContainer!!.visibility = View.GONE
        mCheckContainer!!.visibility = View.VISIBLE
        mViewTitle!!.visibility = View.GONE
        mEditTitle!!.visibility = View.VISIBLE
        mMode = EDIT_MODE_ENABLED
        enableContentInteraction()
    }

    private fun disableEditMode() {
        Log.d(TAG, "disableEditMode: called.")
        mBackArrowContainer!!.visibility = View.VISIBLE
        mCheckContainer!!.visibility = View.GONE
        mViewTitle!!.visibility = View.VISIBLE
        mEditTitle!!.visibility = View.GONE
        mMode = EDIT_MODE_DISABLED
        disableContentInteraction()
        var temp = mLinedEditText!!.text.toString()
        temp = temp.replace("\n", "")
        temp = temp.replace(" ", "")
        if (temp.length > 0) {
            mNoteFinal!!.playerName = mEditTitle!!.text.toString()
            Log.d(TAG, "disableEditMode: initial: " + mNoteInitial.toString())
            Log.d(TAG, "disableEditMode: final: " + mNoteFinal.toString())
            // If the note was altered, save it.
            if (mNoteFinal!!.playerName != mNoteInitial.playerName) {
                Log.d(TAG, "disableEditMode: called?")
                saveChanges()
            }
        }
    }

    private fun setNewNoteProperties() {
        mViewTitle!!.text = "Nazwa Drużny"
        mEditTitle!!.setText("Nazwa drużyny")
        mNoteFinal = Player()
        mNoteInitial = Player()
        mNoteInitial.playerName = "Nazwa drużyny"
    }

    private fun setNoteProperties() {
        mViewTitle!!.text = mNoteInitial.playerName
        mEditTitle!!.setText(mNoteInitial.playerName)
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        return mGestureDetector!!.onTouchEvent(motionEvent)
    }

    override fun onSingleTapConfirmed(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onDoubleTap(motionEvent: MotionEvent): Boolean {
        Log.d(TAG, "onDoubleTap: double tapped.")
        enableEditMode()
        return false
    }

    override fun onDoubleTapEvent(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onDown(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onShowPress(motionEvent: MotionEvent) {}
    override fun onSingleTapUp(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onScroll(motionEvent: MotionEvent, motionEvent1: MotionEvent, v: Float, v1: Float): Boolean {
        return false
    }

    override fun onLongPress(motionEvent: MotionEvent) {}
    override fun onFling(motionEvent: MotionEvent, motionEvent1: MotionEvent, v: Float, v1: Float): Boolean {
        return false
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.toolbar_back_arrow -> {
                finish()
            }
            R.id.toolbar_check -> {
                disableEditMode()
            }
            R.id.note_text_title -> {
                enableEditMode()
                mEditTitle!!.requestFocus()
                mEditTitle!!.setSelection(mEditTitle!!.length())
            }
        }
    }

    override fun onBackPressed() {
        if (mMode == EDIT_MODE_ENABLED) {
            onClick(mCheck!!)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("mode", mMode)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mMode = savedInstanceState.getInt("mode")
        if (mMode == EDIT_MODE_ENABLED) {
            enableEditMode()
        }
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        mViewTitle!!.text = charSequence.toString()
    }

    override fun afterTextChanged(editable: Editable) {}

    companion object {
        private const val TAG = "NoteActivity"
        private const val EDIT_MODE_ENABLED = 1
        private const val EDIT_MODE_DISABLED = 0
    }
}