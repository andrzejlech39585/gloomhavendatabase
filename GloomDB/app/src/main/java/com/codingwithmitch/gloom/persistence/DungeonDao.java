package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.codingwithmitch.gloom.models.Dungeon;

import java.util.List;

@Dao
public interface DungeonDao {

    @Insert
    long[] insertDungeon(Dungeon... dungeons);

    @Query("SELECT * FROM dungeons")
    LiveData<List<Dungeon>> getDungeons();

    @Delete
    int delete(Dungeon... dungeons);

    @Update
    int updateNotes(Dungeon... dungeons);
}
