package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.codingwithmitch.gloom.models.Team;

import java.util.List;

@Dao
public interface TeamsDao {

    @Insert
    long[] insertTeams(Team... teams);

    @Query("SELECT * FROM teams")
    LiveData<List<Team>> getTeams();

    @Delete
    int delete(Team... teams);

    @Update
    int updateNotes(Team... teams);
}
