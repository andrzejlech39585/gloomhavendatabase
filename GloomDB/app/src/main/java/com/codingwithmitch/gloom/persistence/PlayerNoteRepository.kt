package com.codingwithmitch.gloom.persistence

import android.arch.lifecycle.LiveData
import android.content.Context
import com.codingwithmitch.gloom.async.PlayerDeleteAsyncTask
import com.codingwithmitch.gloom.async.PlayerInsertAsyncTask
import com.codingwithmitch.gloom.async.PlayerUpdateAsyncTask
import com.codingwithmitch.gloom.models.Player

class PlayerNoteRepository(context: Context?) {
    private val mNoteDatabase: TeamDatabase = TeamDatabase.getInstance(context)
    fun insertPlayerTask(player: Player?) {
        PlayerInsertAsyncTask(mNoteDatabase.playerDao).execute(player)
    }

    fun updatePlayerTask(player: Player?) {
        PlayerUpdateAsyncTask(mNoteDatabase.playerDao).execute(player)
    }

    fun retrievePlayerTask(): LiveData<List<Player>> {
        return mNoteDatabase.playerDao.players
    }

    fun deletePlayerTask(player: Player?) {
        PlayerDeleteAsyncTask(mNoteDatabase.playerDao).execute(player)
    }

}