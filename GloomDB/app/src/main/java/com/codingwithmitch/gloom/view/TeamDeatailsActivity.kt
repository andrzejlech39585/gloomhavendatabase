package com.codingwithmitch.gloom.view

import android.os.Bundle
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.OnDoubleTapListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.EditText
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.boss.gloom.R
import com.codingwithmitch.gloom.adapters.PlayersRecyclerAdapter
import com.codingwithmitch.gloom.adapters.TeamsRecyclerAdapter
import com.codingwithmitch.gloom.models.Player
import com.codingwithmitch.gloom.models.Team
import com.codingwithmitch.gloom.persistence.NoteRepository
import com.codingwithmitch.gloom.persistence.PlayerNoteRepository
import com.codingwithmitch.gloom.util.LinedEditText
import com.codingwithmitch.gloom.util.Utility
import com.codingwithmitch.gloom.util.VerticalSpacingItemDecorator
import java.util.ArrayList


class TeamDetailsActivity : AppCompatActivity(), OnTouchListener,PlayersRecyclerAdapter.OnPlayerListener, GestureDetector.OnGestureListener, OnDoubleTapListener, View.OnClickListener, TextWatcher {
    // UI components
    private var mLinedEditText: LinedEditText? = null
    private var mEditTitle: EditText? = null
    private var mViewTitle: TextView? = null
    private var mCheckContainer: RelativeLayout? = null
    private var mBackArrowContainer: RelativeLayout? = null
    private var mCheck: ImageButton? = null
    private var mBackArrow: ImageButton? = null
    // vars
    private var mIsNewNote = false
    lateinit var mNoteInitial: Team
    private var mGestureDetector: GestureDetector? = null
    private var mMode = 0
    private var mNoteRepository: NoteRepository? = null
    private var mNoteFinal: Team? = null

    // ui components
    private var mRecyclerView: RecyclerView? = null
    // vars
    private val mPlayers = ArrayList<Player>()
    private var mPlayerRecyclerAdapter: PlayersRecyclerAdapter? = null
    private var mPlayerRepository: PlayerNoteRepository? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.team_details)
        mEditTitle = findViewById(R.id.note_edit_title)
        mViewTitle = findViewById(R.id.note_text_title)
        mCheck = findViewById(R.id.toolbar_check)
        mBackArrow = findViewById(R.id.toolbar_back_arrow)
        mCheckContainer = findViewById(R.id.check_container)
        mBackArrowContainer = findViewById(R.id.back_arrow_container)
        mNoteRepository = NoteRepository(this)
        mRecyclerView = findViewById(R.id.recyclerViewPlayers)
        findViewById<View>(R.id.fabD).setOnClickListener(this)
        initRecyclerView()
        mNoteRepository = NoteRepository(this)
        retrieveNotes()
        title = getString(R.string.dashboard_teams)
        setListeners()
        if (incomingIntent) {
            setNewNoteProperties()
            enableEditMode()
        } else {
            setNoteProperties()
        }
    }
    private fun retrieveNotes() {
//        mPlayerRepository!!.retrievePlayerTask().observe(this, Observer { players ->
//            if (mPlayers.size > 0) {
//                mPlayers.clear()
//            }
//            if (players != null) {
//              //  mPlayers.addAll(players)
//            }
////            mPlayerRecyclerAdapter!!.notifyDataSetChanged()
//        })
    }

    override fun onPlayerClick(position: Int) {
//        val intent = Intent(this, TeamDetailsActivity::class.java)
//        intent.putExtra("selected_note", mPlayers[position])
//        startActivity(intent)
    }

    private fun initRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = linearLayoutManager
        val itemDecorator = VerticalSpacingItemDecorator(10)
        mRecyclerView!!.addItemDecoration(itemDecorator)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView)
        mPlayerRecyclerAdapter = PlayersRecyclerAdapter(mPlayers, this)
        mRecyclerView!!.adapter = mPlayerRecyclerAdapter
    }

    var itemTouchHelperCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        private fun deletePlayer(player: Player) {
            mPlayers.remove(player)
            mPlayerRecyclerAdapter!!.notifyDataSetChanged()
            mPlayerRepository!!.deletePlayerTask(player)
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            deletePlayer(mPlayers[viewHolder.adapterPosition])
        }
    }

    private fun saveChanges() {
        if (mIsNewNote) {
            saveNewTeam()
        } else {
            updateTeam()
        }
    }

    fun updateTeam() {
        mNoteRepository!!.updateTeamTask(mNoteFinal)
    }

    fun saveNewTeam() {
        mNoteRepository!!.insertTeamTask(mNoteFinal)
    }

    private fun setListeners() {
        mGestureDetector = GestureDetector(this, this)
        mCheck!!.setOnClickListener(this)
        mViewTitle!!.setOnClickListener(this)
        mBackArrow!!.setOnClickListener(this)
        mEditTitle!!.addTextChangedListener(this)
    }

    private val incomingIntent: Boolean
        private get() {
            if (intent.hasExtra("selected_note")) {
                mNoteInitial = intent.getParcelableExtra("selected_note")
                mNoteFinal = Team()
                mNoteFinal!!.partyName = mNoteInitial.partyName
                mNoteFinal!!.id = mNoteInitial.id
                mMode = EDIT_MODE_ENABLED
                mIsNewNote = false
                return false
            }
            mMode = EDIT_MODE_ENABLED
            mIsNewNote = true
            return true
        }



    private fun enableEditMode() {
        mBackArrowContainer!!.visibility = View.GONE
        mCheckContainer!!.visibility = View.VISIBLE
        mViewTitle!!.visibility = View.GONE
        mEditTitle!!.visibility = View.VISIBLE
        mMode = EDIT_MODE_ENABLED
    }

    private fun disableEditMode() {
        Log.d(TAG, "disableEditMode: called.")
        mBackArrowContainer!!.visibility = View.VISIBLE
        mCheckContainer!!.visibility = View.GONE
        mViewTitle!!.visibility = View.VISIBLE
        mEditTitle!!.visibility = View.GONE
        mMode = EDIT_MODE_DISABLED
        mNoteFinal!!.partyName = mEditTitle!!.text.toString()
        saveChanges()
    }

    private fun setNewNoteProperties() {
        mViewTitle!!.text = "Nazwa Drużny"
        mEditTitle!!.setText("Nazwa drużyny")
        mNoteFinal = Team()
        mNoteInitial = Team()
        mNoteInitial.partyName = "Nazwa drużyny"
        saveChanges()
    }

    private fun setNoteProperties() {
//        mViewTitle!!.text = mNoteInitial.partyName
        mEditTitle!!.setText(mNoteInitial.partyName)
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        return mGestureDetector!!.onTouchEvent(motionEvent)
    }

    override fun onSingleTapConfirmed(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onDoubleTap(motionEvent: MotionEvent): Boolean {
        Log.d(TAG, "onDoubleTap: double tapped.")
        enableEditMode()
        return false
    }

    override fun onDoubleTapEvent(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onDown(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onShowPress(motionEvent: MotionEvent) {}
    override fun onSingleTapUp(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onScroll(motionEvent: MotionEvent, motionEvent1: MotionEvent, v: Float, v1: Float): Boolean {
        return false
    }

    override fun onLongPress(motionEvent: MotionEvent) {}
    override fun onFling(motionEvent: MotionEvent, motionEvent1: MotionEvent, v: Float, v1: Float): Boolean {
        return false
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.toolbar_back_arrow -> {
                finish()
            }
            R.id.toolbar_check -> {
                disableEditMode()
            }
            R.id.note_text_title -> {
                enableEditMode()
                mEditTitle!!.requestFocus()
                mEditTitle!!.setSelection(mEditTitle!!.length())
            }
        }
    }

    override fun onBackPressed() {
        if (mMode == EDIT_MODE_ENABLED) {
            onClick(mCheck!!)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("mode", mMode)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mMode = savedInstanceState.getInt("mode")
        if (mMode == EDIT_MODE_ENABLED) {
            enableEditMode()
        }
    }
    interface OnPlayerListener {
        fun onPlayerClick(position: Int)
    }


    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        mViewTitle!!.text = charSequence.toString()
    }

    override fun afterTextChanged(editable: Editable) {}

    companion object {
        private const val TAG = "NoteActivity"
        private const val EDIT_MODE_ENABLED = 1
        private const val EDIT_MODE_DISABLED = 0
    }
}