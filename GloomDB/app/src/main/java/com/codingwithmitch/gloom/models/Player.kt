package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "players")
class Player : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "PlayerName")
    var playerName: String? = null


    constructor(playerName: String?) {
        this.playerName = playerName
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        playerName = `in`.readString()
    }

    override fun toString(): String {
        return "Note{" +
                "PlayerName='" + playerName + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(playerName)
    }

    companion object CREATOR : Parcelable.Creator<Player> {
        override fun createFromParcel(parcel: Parcel): Player {
            return Player(parcel)
        }

        override fun newArray(size: Int): Array<Player?> {
            return arrayOfNulls(size)
        }
    }
}
