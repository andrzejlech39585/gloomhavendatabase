package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "dungeons")
class Dungeon : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "DungeonName")
    var dungeonName: String? = null


    constructor(dungeonName: String?) {
        this.dungeonName = dungeonName
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        dungeonName = `in`.readString()
    }

    override fun toString(): String {
        return "Note{" +
                "DungeonName='" + dungeonName + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(dungeonName)
    }

    companion object CREATOR : Parcelable.Creator<Dungeon> {
        override fun createFromParcel(parcel: Parcel): Dungeon {
            return Dungeon(parcel)
        }

        override fun newArray(size: Int): Array<Dungeon?> {
            return arrayOfNulls(size)
        }
    }
}
