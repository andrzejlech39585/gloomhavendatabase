package com.codingwithmitch.gloom.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.boss.gloom.R
import com.codingwithmitch.gloom.models.Team
import java.util.*

class TeamsRecyclerAdapter(mNotes: ArrayList<Team>, onTeamListener: OnTeamListener) : RecyclerView.Adapter<TeamsRecyclerAdapter.ViewHolder>() {
    private var mTeams = ArrayList<Team>()
    private val mOnTeamListener: OnTeamListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.team_item, parent, false)
        return ViewHolder(view, mOnTeamListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.dungeonID.text = mTeams[position].dungeonId.toString()
            holder.teamName.text = mTeams[position].partyName
        } catch (e: NullPointerException) {
            Log.e(TAG, "onBindViewHolder: Null Pointer: " + e.message)
        }
    }

    override fun getItemCount(): Int {
        return mTeams.size
    }

    inner class ViewHolder(itemView: View, onTeamListener: OnTeamListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var dungeonID: TextView
        var teamName: TextView
        var mOnTeamListener: OnTeamListener
        override fun onClick(view: View) {
            Log.d(TAG, "onClick: $adapterPosition")
            mOnTeamListener.onTeamClick(adapterPosition)
        }

        init {
            dungeonID = itemView.findViewById(R.id.note_timestamp)
            teamName = itemView.findViewById(R.id.note_title)
            mOnTeamListener = onTeamListener
            itemView.setOnClickListener(this)
        }
    }

    interface OnTeamListener {
        fun onTeamClick(position: Int)
    }

    companion object {
        private const val TAG = "NotesRecyclerAdapter"
    }

    init {
        this.mTeams = mNotes
        mOnTeamListener = onTeamListener
    }
}