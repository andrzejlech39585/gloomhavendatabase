package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "teams")
class Team : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0
    @ColumnInfo(name = "PartyName")
    var partyName: String? = null
    @ColumnInfo(name = "DungeonID")
    var dungeonId: Int? = 0
    @ColumnInfo(name = "Reputation")
    var reputation: Int? = null

    constructor(partyName: String?, dungeonId: Int?,reputation: Int?) {
        this.partyName = partyName
        this.dungeonId = dungeonId
        this.reputation = reputation
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        partyName = `in`.readString()
        dungeonId = `in`.readInt()
        reputation = `in`.readInt()
    }

    override fun toString(): String {
        return "Note{" +
                "PartyName='" + partyName + '\'' +
                ", dungeonId='" + dungeonId + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(partyName)
        dungeonId?.let { parcel.writeInt(it) }
        reputation?.let { parcel.writeInt(it) }
    }

    companion object CREATOR : Parcelable.Creator<Team> {
        override fun createFromParcel(parcel: Parcel): Team {
            return Team(parcel)
        }

        override fun newArray(size: Int): Array<Team?> {
            return arrayOfNulls(size)
        }
    }
}
