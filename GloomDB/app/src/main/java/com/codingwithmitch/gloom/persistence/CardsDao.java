package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.codingwithmitch.gloom.models.Card;
import com.codingwithmitch.gloom.models.Team;
import com.codingwithmitch.gloom.models.Team;

import java.util.List;

@Dao
public interface CardsDao {

    @Insert
    long[] insertCards(Card... cards);

    @Query("SELECT * FROM cards")
    LiveData<List<Card>> getCards();

    @Delete
    int delete(Card... cards);

    @Update
    int updateNotes(Card... cards);
}
