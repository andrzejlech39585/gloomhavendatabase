package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.codingwithmitch.gloom.models.Item;

import java.util.List;

@Dao
public interface ItemDao {

    @Insert
    long[] insertClazz(Item... items);

    @Query("SELECT * FROM items")
    LiveData<List<Item>> getItems();

    @Delete
    int delete(Item... items);

    @Update
    int updateNotes(Item... items);
}
