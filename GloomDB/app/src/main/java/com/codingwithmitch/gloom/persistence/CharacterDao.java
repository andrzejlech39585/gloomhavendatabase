package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.codingwithmitch.gloom.models.Character;

import java.util.List;

@Dao
public interface CharacterDao {

    @Insert
    long[] insertCharacters(Character... characters);

    @Query("SELECT * FROM characters")
    LiveData<List<Character>> getCharacters();

    @Delete
    int delete(Character... characters);

    @Update
    int updateNotes(Character... cards);
}
