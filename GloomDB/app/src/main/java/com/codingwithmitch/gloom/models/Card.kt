package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "cards")
class Card : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "CardName")
    var cardName: String? = null

    @ColumnInfo(name = "upperProperty")
    var upperProperty: String? = null

    @ColumnInfo(name = "Initiative")
    var initiative: Int? = 0

    @ColumnInfo(name = "lowerProperty")
    var lowerProperty: String? = null

    @ColumnInfo(name = "cardLevel")
    var cardLevel: Int? = 0

    @ColumnInfo(name = "classId")
    var classId: Int? = 0

    constructor(cardName: String?, upperProperty: String?, initiative: Int?, lowerProperty: String?, cardLevel: Int?, classId: Int?) {
        this.cardName = cardName
        this.upperProperty = upperProperty
        this.initiative = initiative
        this.lowerProperty = lowerProperty
        this.cardLevel = cardLevel
        this.classId = classId
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        cardName = `in`.readString()
        upperProperty = `in`.readString()
        initiative = `in`.readInt()
        lowerProperty = `in`.readString()
        cardLevel = `in`.readInt()
        classId = `in`.readInt()
    }

    override fun toString(): String {
        return "Note{" +
                "CardName=' $cardName '\'' " +
                ", UpperProperty='$upperProperty' '\''" +
                ", Initiative='$initiative' '\''" +
                ", LowerProperty='$lowerProperty' '\''" +
                ", CardLevel= '$cardLevel' '\''" +
                ", ClassId= '$classId' '\''" +
                "''}'"
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(cardName)
        parcel.writeString(upperProperty)
        initiative?.let { parcel.writeInt(it) }
        parcel.writeString(lowerProperty)
        cardLevel?.let { parcel.writeInt(it) }
        classId?.let { parcel.writeInt(it) }
    }

    companion object CREATOR : Parcelable.Creator<Card> {
        override fun createFromParcel(parcel: Parcel): Card {
            return Card(parcel)
        }

        override fun newArray(size: Int): Array<Card?> {
            return arrayOfNulls(size)
        }
    }
}
