package com.codingwithmitch.gloom.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.boss.gloom.R
import com.codingwithmitch.gloom.models.Player
import com.codingwithmitch.gloom.models.Team
import com.codingwithmitch.gloom.util.Utility
import java.util.*

class PlayersRecyclerAdapter(mNotes: ArrayList<Player>, onPlayerListener: OnPlayerListener) : RecyclerView.Adapter<PlayersRecyclerAdapter.ViewHolder>() {
    private var mPlayers = ArrayList<Player>()
    private val mOnPlayerListener: OnPlayerListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.player_item, parent, false)
        return ViewHolder(view, mOnPlayerListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            var name = mPlayers[position].playerName!!
        } catch (e: NullPointerException) {
            Log.e(TAG, "onBindViewHolder: Null Pointer: " + e.message)
        }
    }

    override fun getItemCount(): Int {
        return mPlayers.size
    }

    inner class ViewHolder(itemView: View, onPlayerListener: OnPlayerListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var name: TextView
        var mOnPlayerListener: OnPlayerListener
        override fun onClick(view: View) {
            Log.d(TAG, "onClick: $adapterPosition")
            mOnPlayerListener.onPlayerClick(adapterPosition)
        }

        init {
            name = itemView.findViewById(R.id.note_title)
            mOnPlayerListener = onPlayerListener
            itemView.setOnClickListener(this)
        }
    }

    interface OnPlayerListener {
        fun onPlayerClick(position: Int)
    }

    companion object {
        private const val TAG = "NotesRecyclerAdapter"
    }

    init {
        this.mPlayers = mNotes
        mOnPlayerListener= onPlayerListener
    }
}