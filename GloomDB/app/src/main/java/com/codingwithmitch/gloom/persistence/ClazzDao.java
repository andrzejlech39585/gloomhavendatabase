package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.codingwithmitch.gloom.models.Clazz;

import java.util.List;

@Dao
public interface ClazzDao {

    @Insert
    long[] insertClazz(Clazz... clazzes);

    @Query("SELECT * FROM clazzes")
    LiveData<List<Clazz>> getClasses();

    @Delete
    int delete(Clazz... clazzes);

    @Update
    int updateNotes(Clazz... clazzes);
}
