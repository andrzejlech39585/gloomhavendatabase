package com.codingwithmitch.gloom.persistence

import android.arch.lifecycle.LiveData
import android.content.Context
import com.codingwithmitch.gloom.async.DeleteAsyncTaskTeam
import com.codingwithmitch.gloom.async.InsertAsyncTaskTeam
import com.codingwithmitch.gloom.async.UpdateAsyncTaskTeam
import com.codingwithmitch.gloom.models.Team

class NoteRepository(context: Context?) {
    private val mNoteDatabase: TeamDatabase = TeamDatabase.getInstance(context)
    fun insertTeamTask(team: Team?) {
        InsertAsyncTaskTeam(mNoteDatabase.noteDao).execute(team)
    }

    fun updateTeamTask(team: Team?) {
        UpdateAsyncTaskTeam(mNoteDatabase.noteDao).execute(team)
    }

    fun retrieveTeamsTask(): LiveData<List<Team>> {
        return mNoteDatabase.noteDao.teams
    }

    fun deleteTeamTask(team: Team?) {
        DeleteAsyncTaskTeam(mNoteDatabase.noteDao).execute(team)
    }

}