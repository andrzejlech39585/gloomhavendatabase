package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.ParcelFileDescriptor
import android.os.Parcelable

@Entity(tableName = "items")
class Item : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "ItemName")
    var itemName: String? = null

    @ColumnInfo(name = "ItemDescription")

    var itemDescription: String? = null

    @ColumnInfo(name = "ItemPrice")

    var itemPrice: Int? = null

    @ColumnInfo(name = "ItemStateName")

    var itemStateName: String? = null

    @ColumnInfo(name = "ItemTypeName")

    var itemTypeName: String? = null

    constructor(itemName: String?, itemDescription: String? , itemPrice: Int?, itemStateName: String?, itemTypeName: String?) {
        this.itemName = itemName
        this.itemDescription = itemDescription
        this.itemPrice = itemPrice
        this.itemStateName = itemStateName
        this.itemTypeName = itemTypeName
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        itemName = `in`.readString()
        itemDescription = `in`.readString()
        itemPrice = `in`.readInt()
        itemStateName = `in`.readString()
        itemTypeName = `in`.readString()
    }

    override fun toString(): String {
        return "Note{" +
                "ItemName='" + itemName + '\'' +
                ", ItemDescription='" + itemDescription + '\'' +
                ", ItemPrice='" + itemPrice + '\'' +
                ", ItemStateName='" + itemStateName + '\'' +
                ", ItemTypeName='" + itemTypeName + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(itemName)
        parcel.writeString(itemDescription)
        itemPrice?.let { parcel.writeInt(it) }
        parcel.writeString(itemStateName)
        parcel.writeString(itemTypeName)
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }
}
