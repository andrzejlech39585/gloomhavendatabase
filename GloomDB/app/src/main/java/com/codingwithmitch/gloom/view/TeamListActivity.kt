package com.codingwithmitch.gloom.view

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.boss.gloom.R
import com.codingwithmitch.gloom.adapters.TeamsRecyclerAdapter
import com.codingwithmitch.gloom.adapters.TeamsRecyclerAdapter.OnTeamListener
import com.codingwithmitch.gloom.models.Team
import com.codingwithmitch.gloom.persistence.NoteRepository
import com.codingwithmitch.gloom.util.VerticalSpacingItemDecorator

import java.util.*

class TeamListActivity : AppCompatActivity(), OnTeamListener, View.OnClickListener {
    // ui components
    private var mRecyclerView: RecyclerView? = null
    // vars
    private val mNotes = ArrayList<Team>()
    private var mNoteRecyclerAdapter: TeamsRecyclerAdapter? = null
    private var mNoteRepository: NoteRepository? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.team_list)
        mRecyclerView = findViewById(R.id.recyclerView)
        findViewById<View>(R.id.fab).setOnClickListener(this)
        initRecyclerView()
        mNoteRepository = NoteRepository(this)
        retrieveNotes()
        setSupportActionBar(findViewById<View>(R.id.notes_toolbar) as Toolbar)
        title = getString(R.string.dashboard_teams)
    }

    private fun retrieveNotes() {
        mNoteRepository!!.retrieveTeamsTask().observe(this, Observer { teams ->
            if (mNotes.size > 0) {
                mNotes.clear()
            }
            if (teams != null) {
                mNotes.addAll(teams)
            }
            mNoteRecyclerAdapter!!.notifyDataSetChanged()
        })
    }

    private fun initRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = linearLayoutManager
        val itemDecorator = VerticalSpacingItemDecorator(10)
        mRecyclerView!!.addItemDecoration(itemDecorator)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView)
        mNoteRecyclerAdapter = TeamsRecyclerAdapter(mNotes, this)
        mRecyclerView!!.adapter = mNoteRecyclerAdapter
    }

    override fun onTeamClick(position: Int) {
        val intent = Intent(this, TeamDetailsActivity::class.java)
        intent.putExtra("selected_note", mNotes[position])
        startActivity(intent)
    }

    override fun onClick(view: View) {
        val intent = Intent(this, TeamDetailsActivity::class.java)
        startActivity(intent)
    }

    private fun deleteNote(note: Team) {
        mNotes.remove(note)
        mNoteRecyclerAdapter!!.notifyDataSetChanged()
        mNoteRepository!!.deleteTeamTask(note)
    }

    var itemTouchHelperCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            deleteNote(mNotes[viewHolder.adapterPosition])
        }
    }

    companion object {
        private const val TAG = "NotesListActivity"
    }
}