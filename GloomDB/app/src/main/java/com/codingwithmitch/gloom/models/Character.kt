package com.codingwithmitch.gloom.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "characters")
class Character : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id = 0
    @ColumnInfo(name = "CharacterName")
    var characterName: String? = null
    @ColumnInfo(name = "ClassId")
    var classId: Int? = 0
    @ColumnInfo(name = "PlayerId")
    var playerId: Int? = 0

    constructor(characterName: String?, classId: Int?, playerId: Int?) {
        this.characterName = characterName
        this.classId = classId
        this.playerId = playerId
    }

    @Ignore
    constructor() {
    }

    protected constructor(`in`: Parcel) {
        id = `in`.readInt()
        characterName = `in`.readString()
        classId = `in`.readInt()
        playerId = `in`.readInt()
    }

    override fun toString(): String {
        return "Note{" +
                "CharacterName='" + characterName + '\'' +
                ", ClassId='" + classId + '\'' +
                ", PlayerId='" + playerId + '\'' +
                '}'

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(characterName)
        classId?.let { parcel.writeInt(it) }
        playerId?.let { parcel.writeInt(it) }
    }

    companion object CREATOR : Parcelable.Creator<Character> {
        override fun createFromParcel(parcel: Parcel): Character{
            return Character(parcel)
        }

        override fun newArray(size: Int): Array<Character?> {
            return arrayOfNulls(size)
        }
    }
}
