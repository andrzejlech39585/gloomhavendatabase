package com.codingwithmitch.gloom.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.codingwithmitch.gloom.models.Player;

import java.util.List;

@Dao
public interface PlayerDao {

    @Insert
    long[] insertPlayer(Player... players);

    @Query("SELECT * FROM players")
    LiveData<List<Player>> getPlayers();

    @Delete
    int delete(Player... players);

    @Update
    int updateNotes(Player... players);
}
