package com.codingwithmitch.gloom.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.codingwithmitch.gloom.models.Card;
import com.codingwithmitch.gloom.models.Character;
import com.codingwithmitch.gloom.models.Clazz;
import com.codingwithmitch.gloom.models.Dungeon;
import com.codingwithmitch.gloom.models.Item;
import com.codingwithmitch.gloom.models.Player;
import com.codingwithmitch.gloom.models.Team;

@Database(entities = {Team.class, Player.class, Item.class, Dungeon.class, Clazz.class, Character.class, Card.class}, version = 2)
public abstract class TeamDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "teams_db";

    private static TeamDatabase instance;

    static TeamDatabase getInstance(final Context context){
        if(instance == null){
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    TeamDatabase.class,
                    DATABASE_NAME
            ).fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract TeamsDao getNoteDao();

    public abstract PlayerDao getPlayerDao();

}