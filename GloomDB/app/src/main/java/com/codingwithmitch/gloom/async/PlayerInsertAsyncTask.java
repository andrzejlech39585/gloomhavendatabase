package com.codingwithmitch.gloom.async;

import android.os.AsyncTask;

import com.codingwithmitch.gloom.models.Player;
import com.codingwithmitch.gloom.models.Team;
import com.codingwithmitch.gloom.persistence.PlayerDao;
import com.codingwithmitch.gloom.persistence.TeamsDao;

public class PlayerInsertAsyncTask extends AsyncTask<Player, Void, Void> {

    private PlayerDao mPlayersDao;

    public PlayerInsertAsyncTask(PlayerDao dao) {
        mPlayersDao = dao;
    }

    @Override
    protected Void doInBackground(Player... players) {
        mPlayersDao.insertPlayer(players);
        return null;
    }

}